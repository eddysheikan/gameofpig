# Use framework
require 'sinatra'

# Use JSON
require 'json'

# Enable cookies
enable :sessions

# Existing games in the server
$games = {}

# Avoid HTML injection (Not used)
helpers do
  include Rack::Utils
  alias_method :h, :escape_html
end

# Define some resources and methods

# Home
get '/' do
  @title = "The Game of Pig"
  erb :index
end

# User wants to create a game
get '/NewGame' do
  @title = "The Game of Pig"
  erb :newgame
end

# User wants to join an existing game
get '/JoinGame' do
  @title = "The Game of Pig"
  erb :joingame
end

# The Game
get '/Game' do
  @player = session[:player]
  erb :game
end

# How the game is going?
get '/State' do
  puts $games[session[:game]]
  puts session[:player]
  result = obtain_state
  result = {'state' => result, "score1" => $games[session[:game]]["score1"], "score2" => $games[session[:game]]["score2"], "turn_total" => $games[session[:game]]["turn_total"]}
  result.to_json
end

# Create a Game
post '/CreateGame' do
  # Player who creates the game is player 1
  session[:player] = 1
  #Parsing the body -_-
  game_data = request.body.read.split("&").map{|x| x.split(/\w+=/)}.flatten.reject!{|c| c.empty?}
  session[:game] = game_data[0]
  # Create the game
  new_game = {game_data[0] => {"name" => game_data[1], "started" => false, "finished" => false, "score1" => 0, "score2" => 0, "turn_total" => 0, "turn" => 1}}
  # Merging into the global object
  $games.merge!(new_game);
  # Redirect to GET method
  redirect '/Game'
end

# Joining a player to the game
post '/Joining' do
  # Player who joins is player 2
  session[:player] = 2
  #Parsing the body -_-
  session[:game] = request.body.read.split(/\w+=/).reject!{|c| c.empty?}[0]
  # Starting the game (Nobody can join after started)
  $games[session[:game]]["started"] = true;
  # Redirect to GET method  
  redirect '/Game'
end

# Again, the home page
post '/' do
  # Redirect to GET method
  redirect '/'
end

put '/UsingTurn' do
  # The turn (Should continue or not)
  die = 1 + rand(6)
  result = die == 1 ? 'end' : 'continue'
  if die != 1
    $games[session[:game]]["turn_total"] += die
  else
    $games[session[:game]]["turn_total"] = 0
    $games[session[:game]]["turn"] = ($games[session[:game]]["turn"] % 2) + 1
  end  
  {'state' => result, 'number' => die, "turn" => $games[session[:game]]["turn_total"]}.to_json
end

put '/Holding' do
  my_score = "score#{session[:player]}"
  $games[session[:game]][my_score] += $games[session[:game]]["turn_total"]
  if $games[session[:game]][my_score] >= 100
    $games[session[:game]]["finished"] = true
  end
  result = {"turn" => $games[session[:game]]["turn_total"], "score" => my_score}
  $games[session[:game]]["turn_total"] = 0
  result.merge!({"new_score" => $games[session[:game]][my_score]})
  $games[session[:game]]["turn"] = ($games[session[:game]]["turn"] % 2) + 1
  result.to_json
end

# User asked for a non-existant resource
not_found do
  halt 404, erb(:notfound)
end

def obtain_state
  my_score = "score#{session[:player]}"
  return 'win' if $games[session[:game]]["finished"] == true && $games[session[:game]][my_score] >= 100
  return 'lose' if $games[session[:game]]["finished"] == true
  return 'turn' if $games[session[:game]]["started"] == true && $games[session[:game]]["turn"] == session[:player]
  'wait'
end
