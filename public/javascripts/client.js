'use strict'

var PAUSA = 1000;

$(document).ready(function(){
  waitTurn();  
  $("#roll").click(roll)
  $('#hold').click(hold)
  
  //----------------------------------------------------------------------------
  function waitTurn() {
    $('#hold').addClass('invisible')
    deactivate();

    var seconds = 0;

    $('body').css('cursor', 'wait');

    function ticToc() {
      $('#message_2').html('Waiting for ' + seconds + ' second' +
        (seconds === 1 ? '' : 's'));
      seconds++;
      $.ajax({
        url: '/State',
        type: 'GET',
        dataType: 'json',
        error: connectionError,
        success: function(result) {
          
          $("#score1").html(result.score1)
          $("#score2").html(result.score2)
          $("#turn_score").html(result.turn_total)

          switch (result.state) {

          case 'turn':
            turn();
            break;

          case 'wait':
            setTimeout(ticToc, PAUSA);
            break;

          case 'win':
            endGame('<strong>You Won!.</strong> Well Done!');
            break;

          case 'lose':
            endGame('<strong>You lost...</strong> :(');
            break;
          }
        }
      });
    };
    setTimeout(ticToc, 0);
  };

  //----------------------------------------------------------------------------
  
  function endGame(message) {
    $('body').css('cursor', 'auto');
    $('#message_1').html(message);
    $('#message_2').html('');
    $('#to_menu').show();
  }
  
  //----------------------------------------------------------------------------
  
  function turn(tablero) {
    $('body').css('cursor', 'auto');
    $('#message_1').html('It\'s your turn.');
    $('#message_2').html('');
    activate();
  }
  
  //----------------------------------------------------------------------------
  
  function activate(){
    $('#roll').removeClass('invisible')    
  }
  
  //----------------------------------------------------------------------------
  
  function deactivate(){
    $('#roll').addClass('invisible')    
  }
  
  //----------------------------------------------------------------------------
  
  function errorMessage(message) {
    $('body').css('cursor', 'auto');
    $('div').hide();
    $('#error_message').html(message);
    $('#error').show();
  }
  
  //----------------------------------------------------------------------------
  
  function connectionError() {
    errorMessage('Cannot connect to the server. Better nerf Irelia');
  }
  
  //----------------------------------------------------------------------------
  
  function roll() {
    $.ajax({
        url: '/UsingTurn',
        type: 'PUT',
        dataType: 'json',
        error: connectionError,
        success: function(result) {
        
          $("#turn_score").html(result.turn)
        
          switch (result.state) {

          case 'continue':
            $('#message_4').html('You obtained a ' + result.number);
            $('#hold').removeClass('invisible')
            turn();            
            break;

          case 'end':
            $('#message_1').html('You obtained a 1. You have been slain');
            $('#message_4').addClass('invisible')
            waitTurn();
            break;

          }
        }
      });
  }
  
  //----------------------------------------------------------------------------
  
  function hold() {
      $.ajax({
        url: '/Holding',
        type: 'PUT',
        dataType: 'json',
        error: connectionError,
        success: function(result) {
          
          $('#message_1').html('You obtained ' + result.turn + ' points');
          $('#message_4').html('')
          $('#' + result.score).html(result.new_score);
          waitTurn();        
        }
      });
  }
  
})
